# Show url node app

## Description

This is a simple node js application that we will use to make examples using the ingress Google.

As we can see in the `app.js` file :

```js
const express = require("express"),
  app = express();

const version = "node-show-url:v1.0.0";

app.get("/health-check", function(req, res) {
  res.send(`{
    status: "up",
    version: ${version}
  }`);
});

app.get("/*", function(req, res) {
  var fullUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  res.send(`${version} received a request on : ${fullUrl}`);
});

app.listen(8000, function() {
  console.log(`${version} is listening on port 8000 !!!`);
});
```

As we can see this app provides a static route :

- /health-check

the last route `/*` is used to intercept any other request and display it.

## Installation

On the current directory, run this command to build the docker image :

    $ docker build -t eu.gcr.io/[YOUR-PROJECT-ID]/node-show-url-app:v1.0.0 .

To get the list of your projects ids, run this command :

    $ gcloud projects list

Now, run this command to push the image (you have to log in before) :

    $ gcloud docker -- push eu.gcr.io/[YOUR-PROJECT-ID]/node-show-url-app:v1.0.0

We gonna do the same process but using a new version this time. Change the version of this line in the source code :

```js
const version = "node-show-url:v1.0.0";
```

To :

```js
const version = "node-show-url:v2.0.0";
```

Execute these commands :

    $ docker build -t eu.gcr.io/[YOUR-PROJECT-ID]/node-show-url-app:v2.0.0 .

    $ gcloud docker -- push eu.gcr.io/[YOUR-PROJECT-ID]/node-show-ur-appl:v2.0.0

You can check this url to see if the images has been pushed correctly :

- [google container registry](https://console.cloud.google.com/gcr)
