const express = require("express"),
  app = express();

const version = "node-show-url:v1.0.0";

app.get("/health-check", function(req, res) {
  res.send(`{
    status: "up",
    version: ${version}
  }`);
});

app.get("/*", function(req, res) {
  var fullUrl = req.protocol + "://" + req.get("host") + req.originalUrl;
  res.send(`${version} received a request on : ${fullUrl}`);
});

app.listen(8000, function() {
  console.log(`${version} is listening on port 8000 !!!`);
});
