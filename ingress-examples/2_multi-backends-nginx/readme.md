# Nginx Ingress Google

## Description

In this second example we gonna use the nginx ingress controller to deploy the last example and we gonna see the differences between this controller and the google controller.

- [NGINX Ingress Controller](https://kubernetes.github.io/ingress-nginx/)

## Installation

You have to install nginx on your cluster, to do so check the main readme of the project.

**PN** : Don't forget to change `[YOUR-PROJECT-ID]` with your real project id in the deployments files. You can use this command to get your projects ids :

    $ gcloud projects list

After pushing the node app (show-url-node-app) in your container registry, run this command :

    $ kubectl apply -f .

As you can see in the `node-deployment.yaml` and `node-v2-deployment.yaml`, i'm using the same docker container.

You have to add the nginx class annotation in the ingress yaml config file.

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
    - http:
        paths:
          - path: /
            backend:
              serviceName: node-cluster-ip
              servicePort: 8080
          - path: /v2
            backend:
              serviceName: node-v2-cluster-ip
              servicePort: 8080
```

And the routes declaration is not the same as the google ingress. We didn't use the star wildcard.

We have used the rewrite-target annotation because we did not configure a main context on our application we are using the default context which is the root context / .

Check pods :

    $ kubectl get pods

```log
NAME                                     READY   STATUS    RESTARTS   AGE
hello-deployment-copy-57b8945cb7-p2rgd   1/1     Running   0          27s
node-deployment-64f4c6ff89-vzmp7         1/1     Running   0          26s
```

Check services :

    $ kubectl get services

```log
NAME                   TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
kubernetes             ClusterIP   10.55.240.1     <none>        443/TCP    5d21h
node-cluster-ip        ClusterIP   10.55.249.135   <none>        8080/TCP   45s
node-copy-cluster-ip   ClusterIP   10.55.254.148   <none>        8080/TCP   45s
```

Check ingress :

    $ kubectl get ingress

```log
NAME         HOSTS   ADDRESS          PORTS   AGE
my-ingress   *       35.195.200.231   80      72s
```

So fast, the address has been published (it's one of the differences between nginx and google ingresses).

The other difference is that we can't use a static ip here. So, the next time we delete/recreate the project we gonna have a new ip address and it's gonna be problematic if we have users or apps using our project or even a domain name pointing on our ip address.

## Tests

We gonna do some tests now :

our routing rules are :

```yaml
paths:
  - path: /
    backend:
      serviceName: node-cluster-ip
      servicePort: 8080
  - path: /v2
    backend:
      serviceName: node-v2-cluster-ip
      servicePort: 8080
```

### Test of the first route

    $ curl http://35.195.200.231/

```log
node-show-url:v1.0.0 received a request on : http://35.195.200.231/
```

    $ curl http://35.195.200.231/health-check

```json
{
    status: "up",
    version: node-show-url:v1.0.0
}
```

    $ curl http://35.195.200.231/health-check/dummy-route

```log
node-show-url:v1.0.0 received a request on : http://35.195.200.231/health-check/dummy-route
```

### Test of the second route

    $ curl http://35.195.200.231/v2

```log
node-show-url:v2.0.0 received a request on : http://35.195.200.231/
```

As we can see here, the ingress removed the routing context (which is /v2) from the request. This because we are using the url rewriting annotation.

    $ curl http://35.195.200.231/v2/dummy-route

```log
node-show-url:v2.0.0 received a request on : http://35.195.200.231/
```

    $ curl http://35.195.200.231/v2/health-check

```log
node-show-url:v2.0.0 received a request on : http://35.195.200.231/
```

Well, this time the ingress is removing url and replacing it with the root context. We have this behavior because of our rewrite target configuration. We gonna in other examples how to write a config that matches all routes of your application.

## Cleaning up

To clean up your cluster, use this command :

    $ kubectl delete -f .
