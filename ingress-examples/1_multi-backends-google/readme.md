# Ingress Google

## Description

This is the first example in which i will use ingress, go and have a look on the Kubernetes documentation to know in which cases you have to use ingress.

- [ingress resource](https://kubernetes.io/docs/concepts/services-networking/ingress/)

We will create an ingress using Google implementation and we will expose two routes.

## Installation

After pushing the node app in your container registry, run this command :

**PN** : Don't forget to change `[YOUR-PROJECT-ID]` with your real project id the deployment files.

    $ kubectl apply -f .

As you can see in the `node-deployment.yaml` and `node-v2-deployment.yaml`, i'm using the same docker container. Well, it's because i faced some difficulties when i used the google ingress and i will explain one of them using this example.

If you check the ingress yaml file, you will notice that we have used our google static ip.

Check pods :

    $ kubectl get pods

```log
NAME                                     READY   STATUS    RESTARTS   AGE
hello-deployment-copy-6ffb9f6dc9-5h528   1/1     Running   0          62s
node-deployment-8675bd8445-j67sl         1/1     Running   0          61s
```

Check services :

    $ kubectl get services

```log
NAME                  TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
kubernetes            ClusterIP   10.55.240.1     <none>        443/TCP          2d4h
node-copy-node-port   NodePort    10.55.249.116   <none>        8080:32678/TCP   116s
node-node-port        NodePort    10.55.253.120   <none>        8080:30122/TCP   115s
```

Check ingress :

    $ kubectl get ingress

```log
NAME         HOSTS   ADDRESS   PORTS   AGE
my-ingress   *                 80      2m34s
```

We have to wait some minutes.

- [Kubernetes console](https://console.cloud.google.com/kubernetes)

Well i have an error `Error during sync: error running backend syncing routine: googleapi: Error 403: QUOTA_EXCEEDED - Quota 'BACKEND_SERVICES' exceeded. Limit: 5.0 globally.`. It's always a google to have a look on the console google or even check logs from this url :

- [Google logs](https://console.cloud.google.com/logs)

Check the main readme to resolve the issue (when you face it).

After resolving the issue, i executed this command :

    $ kubectl get ingress

```log
NAME         HOSTS   ADDRESS       PORTS   AGE
my-ingress   *       34.98.92.51   80      18m
```

## Tests

Yes, We are using the global ip address. We gonna do some tests now :

our routing rules are :

```yaml
paths:
  - path: /*
    backend:
      serviceName: node-node-port
      servicePort: 8080
  - path: /v2/*
    backend:
      serviceName: node-v2-node-port
      servicePort: 8080
```

### Test of the first route

    $ curl http://34.98.92.51/

```log
node-show-url:v1.0.0 received a request on : http://34.98.92.51/
```

    $ curl http://34.98.92.51/health-check

```json
{
    status: "up",
    version: node-show-url:v1.0.0
}
```

    $ curl http://34.98.92.51/health-check/dummy-route

```log
node-show-url:v1.0.0 received a request on : http://34.98.92.51/health-check/dummy-route
```

### Test of the second route

    $ curl http://34.98.92.51/v2

```log
node-show-url:v1.0.0 received a request on : http://34.98.92.51/v2
```

As we can see this request was intercepted by the first backend and not by the second. It's normal because using GCE /foo and /foo/\* are not the same path (it will be the same if we use another ingress controller like nginx). If you want to fix this problem, you have change your configuration by adding a new route :

```yaml
paths:
  - path: /*
    backend:
      serviceName: node-node-port
      servicePort: 8080
  - path: /v2/*
    backend:
      serviceName: node-v2-node-port
      servicePort: 8080
  - path: /v2
    backend:
      serviceName: node-v2-node-port
      servicePort: 8080
```

    $ curl http://34.98.92.51/v2/dummy-route

```log
node-show-url:v2.0.0 received a request on : http://34.98.92.51/v2/dummy-route
```

    $ curl http://34.98.92.51/v2/health-check

```log
node-show-url:v2.0.0 received a request on : http://34.98.92.51/v2/health-check
```

Surprise !!! the response is not `{status: "up",version: node-show-url:v2.0.0}` like in the first example, that's because the ingress used by google don't rewrite the url. That means that the the global context of the ingress will not be deleted and instead of submitting http://34.98.92.51/health-check to the second backend google submits http://34.98.92.51/v2/health-check. As we didn't declare this route in our node app, this request (/v2/health-check) will be intercepted by the default route (/\*).

- https://github.com/kubernetes/ingress-gce/issues/109

It's not the same behavior if you use other ingress implementations like nginx. We will make an example on the next project.

I tried to add these annotations to the ingress file but i had the same behavior :

```yaml
annotations:
  kubernetes.io/ingress.global-static-ip-name: "my-web-static-ip"
  kubernetes.io/ingress.class: "gce"
  ingress.kubernetes.io/rewrite-target: /
```

## Cleaning up

To clean up your cluster, use this command :

    $ kubectl delete -f .
