# Hello World !!!

## Description

In this example, we will deploy a hello world application on the google cloud using its load balancer. We are using a docker image developed by the google team.

- gcr.io/google-samples/hello-app:1.0

## Installation

On the current directory, type this command :

    $ kubectl apply -f hello-world.yaml

Check the created service status :

    $ kubectl get services

```log
NAME                  TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
hello-load-balancer   LoadBalancer   10.55.242.205   <pending>     8080:32548/TCP   32s
kubernetes            ClusterIP      10.55.240.1     <none>        443/TCP          8m15s
```

The load balancer will create an ip address to get access to your service from internet, as we can see the operation is in progress we have to wait some minutes.
You can even check the google console :

- [https://console.cloud.google.com/kubernetes/discovery?](https://console.cloud.google.com/kubernetes/discovery?)

After some minutes :

```log
NAME                  TYPE           CLUSTER-IP      EXTERNAL-IP    PORT(S)          AGE
hello-load-balancer   LoadBalancer   10.55.242.205   34.76.201.88   8080:32548/TCP   3m12s
kubernetes            ClusterIP      10.55.240.1     <none>         443/TCP          10m
```

Now we can access our service through this ip address :

- [http://34.76.201.88:8080](http://34.76.201.88:8080)

```sh
$ curl http://34.76.201.88:8080

Hello, world!
Version: 1.0.0
Hostname: hello-deployment-586fcbc5b9-n89kg

```

## Cleaning up

To delete the service and the deployment from your cluster, use this command :

    $ kubectl delete -f hello-world.yaml
