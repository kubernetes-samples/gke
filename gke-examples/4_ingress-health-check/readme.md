# Ingress Health Check

## Description

If you use ingress, it's a good practice to add your readiness and liveness probes. These probes will be used by kubernetes to check if you pod is working or not and if he is ready to accept traffic or not.

- [Configure Liveness, Readiness and Startup Probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)

In this example we will deploy an application and we will configure its liveness and readiness probes. These probes is often used with an ingress configuration but in this example we keep it simple and use a load balancer.

You can find the docker image used in the example on my docker hub repo.

- docker.io/mouhamedali/watch-shop-backend-http

You can find the source code on my git repo.

- [gitlab repo](https://gitlab.com/dummies-apps/watch-shop/watch-shop-backend-http)

As we can see in the deploy yaml file, the probes config is :

```yaml
containers:
  - name: node-app
    image: docker.io/mouhamedali/watch-shop-backend-http
    ports:
      - containerPort: 8080
    readinessProbe:
      httpGet:
        path: /healthz
        port: 8080
    livenessProbe:
      httpGet:
        path: /healthz
        port: 8080
```

- Readiness : Readiness probes are designed to let Kubernetes know when your app is ready to serve traffic. Kubernetes makes sure the readiness probe passes before allowing a service to send traffic to the pod. If a readiness probe starts to fail, Kubernetes stops sending traffic to the pod until it passes.
- Liveness : Liveness probes let Kubernetes know if your app is alive or dead. If you app is alive, then Kubernetes leaves it alone. If your app is dead, Kubernetes removes the Pod and starts a new one to replace it.

- https://cloud.google.com/blog/products/gcp/kubernetes-best-practices-setting-up-health-checks-with-readiness-and-liveness-probes

## Installation

You have to log into you cluster before the installation.

On the current directory, type this command :

    $ kubectl apply -f deploy.yaml

Check the pod :

    $ kubectl get pods

```log
NAME                               READY   STATUS    RESTARTS   AGE
node-deployment-7459d94865-mjc5h   1/1     Running   0          71s
```

check the service :

    $ kubectl get services

```log
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP    PORT(S)          AGE
kubernetes     ClusterIP      10.55.240.1     <none>         443/TCP          2d6h
node-service   LoadBalancer   10.55.241.255   34.77.163.95   8080:32121/TCP   2m49s
```

Well, Now we can access our app from the internet.

    $ curl http://34.77.163.95:8080/healthz

```json
{
  "status": "UP",
  "route": "http://34.77.163.95:8080/healthz"
}
```

    $ curl http://34.77.163.95:8080/gifts

```json
[
  {
    "name": "Happy Birthday",
    "price": "25$",
    "description": "Amazon.com eGift Card.",
    "image": "https://m.media-amazon.com/images/I/81gQraBrBZL._SS500_.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "Email",
      "Gift amount": "$1 - $2,000"
    }
  },
  {
    "name": "Welcome Baby",
    "price": "15$",
    "description": "Amazon.com eGift Card.",
    "image": "https://cdn5.vectorstock.com/i/1000x1000/35/79/welcome-baby-card-vector-14713579.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "No",
      "Delivery method": "Mail",
      "Gift amount": "$1 - $4,000"
    }
  },
  {
    "name": "Congratulations",
    "price": "7$",
    "description": "Amazon.com eGift Card.",
    "image": "https://cdn.shopify.com/s/files/1/0786/6519/products/congrats_on_sleeping_with_the_same_person_wedding_card.jpg?v=1532655560",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "PDF download",
      "Gift amount": "$1 - $6,000"
    }
  },
  {
    "name": "Thank you",
    "price": "6.99$",
    "description": "Amazon.com eGift Card.",
    "image": "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto/gigs/108897597/original/2a21b24798a3c3fe4a492ff52ab8ec52b86bc71b/design-awesome-thank-you-card-within-6-hours.jpg",
    "details": {
      "Type": "Digital gift card",
      "Custom message": "Yes",
      "Delivery method": "Email",
      "Gift amount": "$1 - $200"
    }
  }
]
```

## Cleaning up

To clean up your cluster, use this command :

    $ kubectl delete -f .
