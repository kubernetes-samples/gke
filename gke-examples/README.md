# Google Kubernetes Engine Examples

## Description

In this project, we will provide very simple examples (most of them are hello world) to explain some of basic the kubernetes features like :

- how to deploy your first application
- how to deploy a hello world app using ingress
- how to declare multiple ports on your pod
- how to add a health check probe to your app
- how to use volumes

and more ...
