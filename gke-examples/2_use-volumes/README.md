# Use Volumes

## Description

In this example, we will deploy a node application and we will attach a volume to it to store the logs. You can find the docker image on my docker hub repo.

- docker.io/mouhamedali/watch-shop-backend-http

You can find the source code on my git repo.

- [gitlab repo](https://gitlab.com/dummies-apps/watch-shop/watch-shop-backend-http)

Here's the volume config file.

```yml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: node-logs-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

You need to make the difference between a persistent volume and a persistent volume claim.

A PersistentVolumeClaim (PVC) is a request for storage by a user. It is similar to a Pod. Pods consume node resources and PVCs consume PV resources. Pods can request specific levels of resources (CPU and Memory). Claims can request specific size and access modes (e.g., they can be mounted once read/write or many times read-only). (Kubernetes documentation)

So in the last example, we requested a volume of 1 gigabyte.

The access modes are:

- ReadWriteOnce – the volume can be mounted as read-write by a single node
- ReadOnlyMany – the volume can be mounted read-only by many nodes
- ReadWriteMany – the volume can be mounted as read-write by many nodes

## Installation

You have to log into you cluster before the installation.

On the current directory, type this command :

    $ kubectl apply -f .

### PODS

Check pods :

    $ kubectl get pods

```log
NAME                               READY   STATUS              RESTARTS   AGE
node-deployment-66bc6d58f5-m59x8   0/1     ContainerCreating   0          56s
```

Wait until it's ready. Use watch to follow the pod status.

    $ kubectl get pods --watch

```log
NAME                               READY   STATUS    RESTARTS   AGE
node-deployment-66bc6d58f5-m59x8   1/1     Running   0          101s
```

### Volume

    $ kubectl get PersistentVolumeClaim -o wide

```log
NAME            STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE     VOLUMEMODE
node-logs-pvc   Bound    pvc-94b08e3c-2661-4f4d-8cf0-8060f65f0420   1Gi        RWO            standard       4m12s   Filesystem
```

The volume has been created.

- [console storage](https://console.cloud.google.com/kubernetes/storage)

### Service

Check the created service status :

    $ kubectl get services

```log
NAME           TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
kubernetes     ClusterIP      10.55.240.1    <none>         443/TCP          47h
node-service   LoadBalancer   10.55.242.36   34.77.163.95   8080:31939/TCP   3m1s
```

Now we can access our service through this ip address :

    $ curl http://34.77.163.95:8080

```json
{
  "status": "UP",
  "route": "http://34.77.163.95:8080/"
}
```

    $ curl http://34.77.163.95:8080/healthz

```json
{
  "status": "UP",
  "route": "http://34.77.163.95:8080/healthz"
}
```

## Cleaning up

To delete the service and the deployment from your cluster, use this command :

    $ kubectl delete -f .
