# Ingress Basic example

## Description

This is the first example in which i will use Google Cloud ingress, go and have a look on the Kubernetes documentation to know in which cases you have to use ingress.

- [ingress resource](https://kubernetes.io/docs/concepts/services-networking/ingress/)

In this example we will use the google hello app docker image :

- [gcr.io/google-samples/hello-app:2.0](gcr.io/google-samples/hello-app:2.0)

As we can see in the ingress yaml file :

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: hello-ingress
spec:
  backend:
    serviceName: hello-node-port
    servicePort: 8080
```

in this example, we will deploy an ingress without routing rules. It's a hello world example with a default backend. You can find more complex examples in the same repository.

## Installation

You have to log into you cluster before the installation.

On the current directory, type this command :

    $ kubectl apply -f .

Check the pod :

    $ kubectl get pods

```log
NAME                                READY   STATUS    RESTARTS   AGE
hello-deployment-67d557cb85-h9cg6   1/1     Running   0          71s
```

check the service :

    $ kubectl get services

```log
NAME              TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
hello-node-port   NodePort    10.55.249.144   <none>        8080:30073/TCP   114s
kubernetes        ClusterIP   10.55.240.1     <none>        443/TCP          2d2h
```

The used service is a Node Port so there's no external ip.

    $ kubectl get ingress

```log
NAME            HOSTS   ADDRESS       PORTS   AGE
hello-ingress   *       34.98.92.51   80      3m
```

Well, Now we can access our app from the internet.

    $ curl http://34.98.92.51/

```log
Hello, world!
Version: 2.0.0
Hostname: hello-deployment-67d557cb85-h9cg6
```

## Cleaning up

To clean up your cluster, use this command :

    $ kubectl delete -f .
