# Deploy Https app

## Description

Sometimes, you need to deploy an app that exposes two ports : HTTP and HTTPS ports.
In this example, i will show you how to configure multiple ports for application.

You can find the docker image used in the example on my docker hub repo.

- docker.io/mouhamedali/watch-shop-backend-https

This docker image exposes rest services. You can consume these services using the HTTP protocol or the HTTPS. You can find the source code on my git repo.

- [gitlab repo](https://gitlab.com/dummies-apps/watch-shop/watch-shop-backend-https)

As we can see in the deploy yaml file, to configure ports on the service i used the annotation:

```yaml
annotations:
  cloud.google.com/app-protocols: '{"my-https-port":"HTTPS","my-http-port":"HTTP"}'
```

The annotation says that when an HTTP(S) load balancer targets port 80 of the Service, it should use HTTP. And when the load balancer targets port 443 of the Service, it should use HTTPS.

- https://cloud.google.com/kubernetes-engine/docs/concepts/ingress?hl=en

## Installation

You have to log into you cluster before the installation.

On the current directory, type this command :

    $ kubectl apply -f deploy.yaml

Check the pod :

    $ kubectl get pods

```log
NAME                              READY   STATUS    RESTARTS   AGE
node-deployment-c9f8449f6-mcmj2   1/1     Running   0          11s
```

check the service :

    $ kubectl get services

```log
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
kubernetes     ClusterIP      10.55.240.1     <none>        443/TCP                      2d23h
node-service   LoadBalancer   10.55.254.154   <pending>     443:30278/TCP,80:32576/TCP   32s
```

Wait until you have an external ip.

```log
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
kubernetes     ClusterIP      10.55.240.1     <none>          443/TCP                      2d23h
node-service   LoadBalancer   10.55.254.154   34.77.163.180   443:30278/TCP,80:32576/TCP   83s
```

Well, Now we can access our app from the internet.

    $ curl http://34.77.163.180:80/healthz

```json
{
  "status": "UP",
  "route": "http://34.77.163.180/healthz"
}
```

    $ curl https://34.77.163.180:443/healthz

```log
curl: (60) SSL certificate problem: self signed certificate
More details here: https://curl.haxx.se/docs/sslcerts.html

curl failed to verify the legitimacy of the server and therefore could not
establish a secure connection to it. To learn more about this situation and
how to fix it, please visit the web page mentioned above.
```

It's ok, we have this error because we are using a self signed certificate.

    $ curl --insecure https://34.77.163.180:443/healthz

```json
{
  "status": "UP",
  "route": "https://34.77.163.180/healthz"
}
```

The route property is showing the intercepted url. We are using the Https protocol as we can see.

## Cleaning up

To clean up your cluster, use this command :

    $ kubectl delete -f .
